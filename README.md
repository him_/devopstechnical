# DevopsTechnical

For the Devops test

For question 1 check consul folder, for question 2 check packer folder.

**Question 1**

Please use consul-template -template="TPLSCRIPT.tpl:FILNAME.txt" -once to generate the txt file with the requested data.


**Question 2**
For task 2, the setup.sh script can be modified to change the variable passed for the name of the machine.

Tested on virtualbox 6.1.8 on mac

I was a bit unsure if i understood this task correctly with 2 virtual machines with different names and nginx and updating the log entry after the virtual machines name.

Or if you were referring to 1 nginx machine with 2 virtualhosts, with seperate logging for the virtual hosts, i realized this at the end of the task. If that is the case please let me know and i will rectify as soon as possible