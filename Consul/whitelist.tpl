{{ range $key, $pairs := tree "whitelist" | byKey }}{{ $key }}:
-whitelist
{{ range $pair := $pairs }} -Permit  {{ .Value }}
{{ end }}{{ end }}