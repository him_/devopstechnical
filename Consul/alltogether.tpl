{{ range $index, $domain_name := tree "domains" | explode }}
{{ range $domain, $whitelists := tree "whitelist" | byKey }}
{{ if eq $domain $domain_name }}
domain {{ $domain_name }}
whitelist
{{ range $whitelist := $whitelists }} - {{ .Value }}
{{ end }}
{{ end }}
{{ end }}
{{ end }}