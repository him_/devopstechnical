apt-get install nginx -y
sed -i -E "s/access_log (.*?);/access_log \/var\/log\/nginx\/`hostname`_access.log;/" /etc/nginx/nginx.conf
sed -i -E "s/error_log (.*?);/error_log \/var\/log\/nginx\/`hostname`_error.log;/" /etc/nginx/nginx.conf
